
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f7xx_hal.h"
#include "can.h"
#include "can_comm.h"
#include "gpio.h"
#include "event_timer.h"
#include "stdlib.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

extern uint16_t testing;			//importing the variables from can_comm
extern uint32_t accumulatorVoltage;
 extern uint32_t controllerVoltage;


#define voltDiff 5000 //5000 mV, voltage difference margin
#define initialMaxTsVoltage 5000  //max TS when starting off



/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

void precheckTS(CAN_msg *stateMsg);
void sendErrorMsg(CAN_msg *stateMsg, int8_t errorType, uint8_t timesErrorAppeared);


/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_RESET);
  MX_CAN1_Init();
  MX_CAN1_ConfigFilter();
  /* USER CODE BEGIN 2 */

  /*uint8_t errorFlag = 0;
  uint8_t relaySwitched = 0;
  initial thoughts when designing the algorithm*/

  CAN_msg stateMsg;
  stateMsg.id = 0x76C; // 0d1900
  stateMsg.len = 8;
  stateMsg.ide = IDE_STD;
  stateMsg.rtr = RTR_DATA;

  stateMsg.data[0] = 0x0; // Operation state message; 0 - No state, 1 - precharging, 2 - TS+ is closed 3 - error,
  stateMsg.data[1] = 0x0; /* error defining: 0 - No error definition, 1 - Voltage in TS before switching,
  2 - Precharging took more than 5sec, 3 - Voltage difference too big between accmltr and invtr,
  4 - invtr capacitors didn't start charging, 5 - MOSFET error, 6 - Errorhandler, */
  stateMsg.data[2] = 0x0;	/* 1 - the problem appeared once, 2 - the problem appeared twice */
  stateMsg.data[3] = 0x0;	/* The amount of time precharging has taken, 100ms */
  stateMsg.data[4] = 0x0;
  stateMsg.data[5] = 0x0;
  stateMsg.data[6] = 0x0;
  stateMsg.data[7] = 0x0;

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  uint16_t chargingTimeElapsed = 0;

  can_process_input();

  precheckTS(&stateMsg);

  can_process_input(); // one last check to be sure the TS unknown power problem hasn't reappeared

  if(controllerVoltage <= initialMaxTsVoltage)
  {
	  HAL_GPIO_WritePin(Resistor_relay_GPIO_Port, Resistor_relay_Pin, GPIO_PIN_SET);// starts charging the caps through resistor
	  HAL_Delay(300);
	  can_process_input();
	  if(controllerVoltage < initialMaxTsVoltage) //after 300ms time delay the controller should have more than 5 V of potential in it
	  {
		  HAL_GPIO_WritePin(Resistor_relay_GPIO_Port, Resistor_relay_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
		  while(1) // might be a short, so needs to be checked.
		  {
			  sendErrorMsg(&stateMsg,4,1);
			  HAL_Delay(200);
		  }
	  }
	  stateMsg.data[0] = 0x1;
	  stateMsg.data[1] = 0x0;
	  stateMsg.data[2] = 0x0;
	  stateMsg.data[3] = 0x0;
	  can1_transmit(&stateMsg);

	  while(controllerVoltage <= (0.9*accumulatorVoltage))
	  {
		  can_process_input();

		  if(get_event_flag (FLAG_100MS))
		  {
			  chargingTimeElapsed++;
			  stateMsg.data[0] = 0x1;
			  stateMsg.data[1] = 0x0;
			  stateMsg.data[2] = 0x0;
			  stateMsg.data[3] = chargingTimeElapsed;
			  can1_transmit(&stateMsg);
		  }
		  else if(get_event_flag (FLAG_50MS))
		  {
			  HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin); //led blinks 10Hz
		  }
		  if(chargingTimeElapsed >= 50)
		  {
		  	  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET); // led shines if error happens
		  	  HAL_GPIO_WritePin(Resistor_relay_GPIO_Port, Resistor_relay_Pin, GPIO_PIN_RESET);
			  while(1) //precharging took more than 50*100ms = 5 seconds
			  {
				  if(get_event_flag (FLAG_100MS))
				  {
					  chargingTimeElapsed++;
					  stateMsg.data[0] = 0x3;
					  stateMsg.data[1] = 0x2;
					  stateMsg.data[2] = 0x1;
					  stateMsg.data[3] = chargingTimeElapsed;
					  can1_transmit(&stateMsg);
				  }
			  }
		  }
	  }
	  if(controllerVoltage >= (0.9*accumulatorVoltage))
	  {
		  HAL_GPIO_WritePin(HV_____relay_GPIO_Port, HV_____relay_Pin, GPIO_PIN_SET);
		  HAL_Delay(150); //just to make sure the relay has enough time to switch
		  HAL_GPIO_WritePin(Resistor_relay_GPIO_Port, Resistor_relay_Pin, GPIO_PIN_RESET);
		  HAL_Delay(150);
		  can_process_input();
		  if(abs(accumulatorVoltage - controllerVoltage) >= voltDiff) // if the cntrl and accm voltages dont overlap within voltDiff, something's not right
		  {
			  HAL_GPIO_WritePin(HV_____relay_GPIO_Port, HV_____relay_Pin, GPIO_PIN_RESET); //disconnects relays
			  HAL_GPIO_WritePin(Resistor_relay_GPIO_Port, Resistor_relay_Pin, GPIO_PIN_RESET);
			  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET); // led shines if error happens
			  while(1) //won't leave this before hard reset
			  {
				  if(controllerVoltage >= initialMaxTsVoltage)
				  {
					  sendErrorMsg(&stateMsg,5,1);
				  }
				  else
				  {
					  sendErrorMsg(&stateMsg,3,1);
				  }
				  HAL_Delay(200);
			  }
		  }
	  }
	  else  //something's fucked
	  {
		  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
		  while(1) //won't leave this before hard reset
		  {
			  sendErrorMsg(&stateMsg,3,1);
			  HAL_Delay(200);
		  }

	  }


  }
  else // if the unknown power flow in the TS reappears, something is wrong and won't go away
  {
	  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
	  while(1)
	  {
		  stateMsg.data[0] = 0x3;
		  stateMsg.data[1] = 0x1;
		  stateMsg.data[2] = 0x2;
		  can1_transmit(&stateMsg);
		  HAL_Delay(200);
	  }
  }

  /* When the code runs this far, the precharging has been completed, and the pcb is now
   * constantly checking if the accmltr and cntrl voltages are similar(which they should be)
   * LED blinking 1Hz if everything is okay */
  while(1)
  {
	  can_process_input();
	  if((accumulatorVoltage - controllerVoltage) >= voltDiff)
	  {
		  HAL_GPIO_WritePin(HV_____relay_GPIO_Port, HV_____relay_Pin, GPIO_PIN_RESET); //disconnects relays
		  HAL_GPIO_WritePin(Resistor_relay_GPIO_Port, Resistor_relay_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
		  while(1) // This shouldn't happen this far in the cycle. Something is very off in the TS.
		  {
			  if(controllerVoltage >= initialMaxTsVoltage)
			  {
				  sendErrorMsg(&stateMsg, 5, 1);
			  }
			  else
			  {
				  sendErrorMsg(&stateMsg, 3, 1);
			  }
			  HAL_Delay(200);
		  }
	  }
	  stateMsg.data[0] = 0x2;
	  stateMsg.data[1] = 0x0;
	  stateMsg.data[2] = 0x0;
	  stateMsg.data[3] = 0x0;
	  can1_transmit(&stateMsg);
	  if(get_event_flag (FLAG_500MS))
	  {
	  	  HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
	  }
	  HAL_Delay(200);

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 216;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Activate the Over-Drive mode 
    */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

void precheckTS(CAN_msg *stateMsg) // before mcu closes any relays, there must not be more than 1000mV of potential
{
	uint16_t n = 0;
	while(controllerVoltage >= 1000)
	{
	  can_process_input(); // checks if the problem has been eliminated and exits loop if so
	  sendErrorMsg(stateMsg, 1, n);
	  n++;
	  HAL_Delay(100);
	}
	stateMsg->data[0] = 0x0;
	stateMsg->data[1] = 0x0;
}


/*
 * errorType - the appeared error's type
 * timesErrorAppeared - self-explaining name
 *
 * */

void sendErrorMsg(CAN_msg *stateMsg, int8_t errorType, uint8_t timesErrorAppeared) //to make the main look cleaner, errormessage
{
	stateMsg->data[0] = 0x3;
	stateMsg->data[1] = errorType;
	stateMsg->data[2] = timesErrorAppeared;
	can1_transmit(stateMsg);
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {

  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
