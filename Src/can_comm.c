/*
 * can_comm.c
 *
 *  Created on: 8. mai 2017
 *      Author: Martin
 */

#include "can_comm.h"
#include "main.h"
#include "stm32f7xx_hal.h"
//#include "usart.h"

int32_t accumulatorVoltage = 0;
int32_t controllerVoltage = 0;
//uint16_t testing = 0;

void can_process_input() {
	while (can1_rx_buffer_length() > 0) {
		CAN_msg msg;
		can1_rx_buffer_dequeue(&msg);
		if(msg.id == 0x522) // IVT result_U1 -> controller voltage
		{
			controllerVoltage = (msg.data[3] << 16) + (msg.data[4] << 8) + msg.data[5];

		}
		else if(msg.id == 0x523) // IVT result_U2 -> accumulator voltage
		{
			accumulatorVoltage = (msg.data[3] << 16) + (msg.data[4] << 8) + msg.data[5];
		}
	}
}

//CAN_msg msg;
//
//msg.id = 0x222;
//msg.ide = CAN_MSG_ID_STD;
//msg.rtr = CAN_MSG_RTR_DATA;
//msg.len = 8;
//
//msg.data[0] = 0x01;
//msg.data[1] = 0x02;
//msg.data[2] = 0x03;
//msg.data[3] = 0x04;
//msg.data[4] = 0x05;
//msg.data[5] = 0x06;
//msg.data[6] = 0x07;
//msg.data[7] = 0x08;
//
//can1_transmit(&msg);
