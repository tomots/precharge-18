/*
 * event_timer.c
 *
 *  Created on: 29. apr 2017
 *      Author: Martin
 */

#include "event_timer.h"

int event_flag_upper_limit[FLAG_COUNT] = {
	1,
	5,
	10,
	20,
	50,
	100,
	200,
	500,
	1000,
	5000
};

void event_timer_init (void) {
	for (int i = 0; i < FLAG_COUNT; i++) {
		event_flag_current_count[i] = 0;
		event_flags[i] = 0;
	}
}

void increment_flag_count (void) {
	for (int i = 0; i < FLAG_COUNT; i++) {
		event_flag_current_count[i]++;

		if (event_flag_current_count[i] >= event_flag_upper_limit[i]) {
			event_flag_current_count[i] = 0;
			event_flags[i] = 1;
		}
	}
}

int get_event_flag (EVENT_FLAG flag_id) {
	if (event_flags[flag_id] == 1) {
		event_flags[flag_id] = 0;
		return 1;
	}
	return 0;
}
