/*
 * event_timer.h
 *
 *  Created on: 29. apr 2017
 *      Author: Martin
 */

#ifndef EVENT_TIMER_H_
#define EVENT_TIMER_H_

typedef enum {
	FLAG_1MS,
	FLAG_5MS,
	FLAG_10MS,
	FLAG_20MS,
	FLAG_50MS,
	FLAG_100MS,
	FLAG_200MS,
	FLAG_500MS,
	FLAG_1000MS,
	FLAG_5000MS,

	FLAG_COUNT
}EVENT_FLAG;

int event_flag_current_count[FLAG_COUNT];

int event_flags[FLAG_COUNT];

void event_timer_init (void);

void increment_flag_count (void);

int get_event_flag (EVENT_FLAG);


#endif /* EVENT_TIMER_H_ */
