/*
 * can_comm.h
 *
 *  Created on: 8. mai 2017
 *      Author: Martin
 */

#ifndef CAN_COMM_H_
#define CAN_COMM_H_

#include "can.h"

void can_process_input();

//uint16_t get_battery_voltage();
///* BMS Element */
//uint16_t get_elementVoltage_maximum();
//uint16_t get_elementVoltage_minimum();
//uint16_t get_elementVoltage_average();
//uint8_t get_elementTemperature_maximum();
//uint8_t get_elementTemperature_minimum();
//uint8_t get_elementTemperature_average();
///* BMS Battery */
//uint16_t get_batteryVoltage();
//uint16_t get_batteryTemperature_average();
//uint16_t get_batteryCurrent();
//uint16_t get_batteryEnergyUsed();
///* DC/DC Converter */
//int16_t get_transformerTemperature();
//int16_t get_hBridgeTemperature();
///* Sensorics */
//uint8_t get_selectedMode();
//uint8_t get_settingKnob1();
//uint8_t get_settingKnob2();
///* AMK Controller */
//uint16_t get_AMK_Status_FL();
//uint16_t get_AMK_Status_FR();
//uint16_t get_AMK_Status_RL();
//uint16_t get_AMK_Status_RR();
//uint16_t get_AMK_Errorinfo_FL();
//uint16_t get_AMK_Errorinfo_FR();
//uint16_t get_AMK_Errorinfo_RL();
//uint16_t get_AMK_Errorinfo_RR();
//int16_t get_AMK_motorTemperature_FL();
//int16_t get_AMK_motorTemperature_FR();
//int16_t get_AMK_motorTemperature_RL();
//int16_t get_AMK_motorTemperature_RR();
//int16_t get_AMK_inverterTemperature_FL();
//int16_t get_AMK_inverterTemperature_FR();
//int16_t get_AMK_inverterTemperature_RL();
//int16_t get_AMK_inverterTemperature_RR();
//int16_t get_AMK_igbtTemperature_FL();
//int16_t get_AMK_igbtTemperature_FR();
//int16_t get_AMK_igbtTemperature_RL();
//int16_t get_AMK_igbtTemperature_RR();
//uint8_t get_selectedParameter();
//int8_t get_selectedParameterValue();
//uint8_t get_DriveMode();
///* LAP TIME */
//uint16_t get_beaconLapTimeTrigger();
//uint8_t get_logStatus();
//uint8_t get_is4GUp();
//uint8_t get_limpModeActive();
//int16_t get_regen_kwh();
//int16_t get_energy_total_kwh();
//uint8_t get_power();
//uint8_t get_limpMode_RegenActive();
#endif /* CAN_COMM_H_ */
